
/*
 * Copyright 2008 2009 2010 2011 2012 2013 2014 2015 Douglas Wikstrom
 *
 * This file is part of Verificatum.
 *
 * Verificatum is NOT free software. It is distributed under the
 * Verificatum Research License. You should have agreed to this
 * license when downloading Verificatum and received a copy of the
 * license along with Verificatum. If not, then see
 * <http://www.verificatum.org/VERIFICATUM_RESEARCH_LICENSE>. If you
 * do not agree to this license, then you may not use Verificatum in
 * any way and you must delete Verificatum.
 */

#include <stdio.h>
#include <gmp.h>

int main()
{
  puts(__GMP_CFLAGS);
}
